import React from "react";
import "./jumbotron.scss";
import heroimg from "../assets/bg-img.jpg";
import { Carousel } from "react-bootstrap";

const Jumbotron = () => {
  return (
    // <div className=" jumbotron justify-content-center">
    <div className="col-md">
      <Carousel>
        <Carousel.Item interval={1000}>
          <img
            className="d-block w-100"
            src="https://source.unsplash.com/178j8tJrNlc/640x420"
            alt="First slide"
          />
          <Carousel.Caption>
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://source.unsplash.com/TRCJ-87Yoh0/640x420"
            alt="Third slide"
          />
          <Carousel.Caption>
            <h3>Third slide label</h3>
            <p>
              Praesent commodo cursus magna, vel scelerisque nisl consectetur.
            </p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </div>
    // </div>
  );
};

export default Jumbotron;
