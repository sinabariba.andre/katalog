import React from "react";

function Layout({ children }) {
  return <main className="container-fluid">{children}</main>;
}
export default Layout;
