import React from "react";

import "./footer.scss";

// import P from "../atoms/P";

const Footer = () => {
  return (
    <div className="footer">
      <p>© 2021 PT Iseng Aja All Rights Reserved.</p>
      <p>All other trademarks belong to their respective owners.</p>
    </div>
  );
};

export default Footer;
